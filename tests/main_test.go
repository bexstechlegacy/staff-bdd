package main

import (
	"db/tests/steps"
	"flag"
	txdb "github.com/DATA-DOG/go-txdb"
	"github.com/cucumber/godog"
	"github.com/cucumber/godog/colors"
	"os"
	"testing"
)

var (
	opts = godog.Options{
		Output: colors.Colored(os.Stdout),
		Format: "progress",
		Paths:  []string{"features"}}
)
var _ = func() bool {
	testing.Init()
	return true
}()

func init() {
	// we register an sql driver txdb
	txdb.Register("txdb", "mysql", "godog:pwd@/godog_test")
	godog.BindFlags("godog.", flag.CommandLine, &opts)

}

func InitializeScenario(ctx *godog.ScenarioContext) {

	steps.NewMainContext(ctx)

}

func TestMain(m *testing.M) {
	opts.Paths = flag.Args()
	flag.Parse()
	status := godog.TestSuite{
		Name:                 "godog-meetup",
		TestSuiteInitializer: InitializeTestSuite,
		ScenarioInitializer:  InitializeScenario,
		Options:              &opts,
	}.Run()

	if st := m.Run(); st > status {
		status = st
	}
	os.Exit(status)
}

func InitializeTestSuite(context *godog.TestSuiteContext) {
}
