package steps

import (
	"fmt"
	"github.com/cucumber/godog"
)

func ParseGherkinMap(table *godog.Table) (map[string]string, error) {
	if len(table.Rows) == 0 {
		return nil, fmt.Errorf("expected table to have at least one row")
	}

	if len(table.Rows[0].Cells) != 2 {
		return nil, fmt.Errorf("expected table to have exactly two columns")
	}

	result := map[string]string{}
	for _, row := range table.Rows {
		result[row.Cells[0].Value] = row.Cells[1].Value
	}

	return result, nil
}
