Feature: users
  In order to use users api
  As an API user
  I need to be able to manage users

  Scenario: should get empty users
    When I send "GET" request to "/users"
    Then the response code should be 200
    And the response should match json:
      """
      {
        "users": []
      }
      """

  Scenario: should get users
    Given there are users:
      | username | email             |
      | john     | john.doe@mail.com |
      | jane     | jane.doe@mail.com |
    When I send "GET" request to "/users"
    Then the response code should be 200
    And the reponse time should be less than 3 seconds
    And the response should not be empty
    And the response should match json:
      """
      {
        "users": [
          {
            "username": "john"
          },
          {
            "username": "jane"
          }
        ]
      }
      """


  Scenario Outline: should get users when there is only one
    Given there are users "<username>" and "<email>"
    When I send "GET" request to "/users"
    Then the response code should be 200
    And the response should match json:
      """
      {
        "users": [
          {
            "username": "gopher"
          }
        ]
      }
      """
    Examples:
      | username | email           |
      | gopher   | gopher@mail.com |
      | gopher2   | gopher2@mail.com |