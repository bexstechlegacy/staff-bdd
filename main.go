package main

import (
	"database/sql"
	"db/server"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
)



type user struct {
	ID       int64  `json:"-"`
	Username string `json:"username"`
	Email    string `json:"-"`
}



func main() {
	db, err := sql.Open("mysql", "godog:pwd@/godog")
	if err != nil {
		panic(err)
	}
	s := &server.Server{Db: db}
	http.HandleFunc("/users", s.Users)
	http.ListenAndServe(":8080", nil)
}


